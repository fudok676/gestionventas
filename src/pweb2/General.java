package pweb2;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pweb2.UsuarioServlet;

public class General extends HttpServlet {

	private static final long serialVersionUID = -6544228691105449366L;

	private static final String NAVIGATION = "/index.jsp"; 
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.insert(req.getParameter("nombres"), req.getParameter("apellidos"),req.getParameter("correo"),req.getParameter("estadoCuenta") );
		resp.sendRedirect(NAVIGATION);
	}
	
	private void insert (final String nombres, final String apellidos,final String correo,final String estadoCuenta) {
		UsuarioServlet.insert(nombres, apellidos, correo,estadoCuenta);
	}
}