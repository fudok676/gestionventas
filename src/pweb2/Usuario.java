package pweb2;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;

import com.google.appengine.api.datastore.Email;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Usuario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3751749015230499002L;
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idUsuario;
	@Persistent
	private String nombres;
	@Persistent
	private String apellidos;
	@Persistent
	@Unique
	private Email correo;
	@Persistent
	private String estadoCuenta;
	@Persistent
	private String idSesion;
	
	public Usuario(String nombres,String apellidos,String correo,String estadoCuenta){
		this.nombres=nombres;
		this.apellidos=apellidos;
		this.correo = new Email(correo);
		this.estadoCuenta=estadoCuenta;	
	}

	public String getIdUsuario() {
		return KeyFactory.keyToString(idUsuario);
	}

	public void setIdUsuario(String correo) {
		Key key = KeyFactory.createKey(Usuario.class.getSimpleName(), correo);
		this.idUsuario = key;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo.getEmail();
	}

	public void setCorreo(String correo) {
		this.correo = new Email(correo);
	}

	
	public String getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}

	
	public String getEstadoCuenta() {
		return estadoCuenta;
	}

	public void setEstadoCuenta(String estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}
	
	

}