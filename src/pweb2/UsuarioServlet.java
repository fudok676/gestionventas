package pweb2;



import javax.jdo.PersistenceManager;
import javax.jdo.Query;


public class UsuarioServlet {

	
	
	/**
	 * Almacenamiento de un nuevo tutorial
	 * @param autor nombre del autor
	 * @param tituloTutorial titulo del tutorial
	 */
	public static void insert(final String nombres, final String apellidos, final String correo, final String estadoCuenta) {
		
		// recuperacion del gestor de persistencia de JDO
		final PersistenceManager persistenceManager = PMF.get().getPersistenceManager();
		
		// creamos un nuevo tutorial y los insertamos en el datastore
		final Usuario usuario = new Usuario(nombres, apellidos, correo,estadoCuenta);		
		persistenceManager.makePersistent(usuario);
	}
	
	/**
	 * Recuperación de los últimos 10 tutoriales (FETCH_MAX_RESULTS)
	 * @return una lista con los últimos 10 tutoriales
	 */
	
}

